#! /usr/bin/env python3

"""Logiciel (rudimentaire) de traitement d'image.

Lancer avec l'argument "all" pour exécuter *tous* les algorithmes (utile pour corriger).

Pour plus d'informations :
https://snt.ababsurdo.fr/la-photographie-numerique/algorithmes/
"""

# pylint: disable=consider-using-f-string, non-ascii-name, unused-variable

import inspect
import logging
import mimetypes
import operator
import os
import pathlib
import sys

from PIL import Image, ImageOps

os.chdir(os.path.dirname(os.path.abspath(__file__)))

DEFAULT_IMAGE = pathlib.Path("20240520-083328.jpg")

################################################################################
# NE RIEN MODIFIER AVANT CETTE LIGNE
################################################################################


def action_trame_rouge(nom):
    """Extrait la trame rouge de l'image"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, y),
                (rouge, 0, 0),  # La trame rouge est copiée, les autres sont supprimées
            )

    dest.save(nom.with_stem(nom.stem + "-trame-rouge"))
    return dest


def action_trame_verte(nom):
    """Extrait la trame verte de l'image"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, y),
                (0, 0, 0),  # À compléter…
            )

    dest.save(nom.with_stem(nom.stem + "-trame-vert"))
    return dest


def action_trame_bleue(nom):
    """Extrait la trame bleue de l'image"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, y),
                (0, 0, 0),  # À compléter…
            )

    dest.save(nom.with_stem(nom.stem + "-trame-bleu"))
    return dest


def action_niveaux_de_gris(nom):
    """Convertit l'image en niveaux de gris."""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            # À compléter…
            dest.putpixel(
                (x, y),
                (0, 0, 0),  # À compléter avec la couleur du pixel
            )

    dest.save(nom.with_stem(nom.stem + "-gris"))
    return dest


def action_permuter(nom):
    """Permute les couleurs"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            # À compléter…
            dest.putpixel(
                (x, y),
                (0, 0, 0),  # À compléter avec la couleur du pixel
            )

    dest.save(nom.with_stem(nom.stem + "-permuter"))
    return dest


def action_symetrie_gauchedroite(nom):
    """Effectue la symérie gauche-droite"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (0, 0),  # À compléter avec la position du pixel
                (0, 0, 0),  # À compléter avec la couleur du pixel
            )

    dest.save(nom.with_stem(nom.stem + "-gauchedroite"))
    return dest


def action_symetrie_hautbas(nom):
    """Effectue la symérie haut-bas"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, hauteur - y - 1),
                (rouge, vert, bleu),
            )

    dest.save(nom.with_stem(nom.stem + "-hautbas"))
    return dest


def action_eclaircir(nom):
    """Éclaircit l'image"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, y),
                (0, 0, 0),  # À compléter avec la couleur du pixel
            )

    dest.save(nom.with_stem(nom.stem + "-eclaircie"))
    return dest


def action_assombrir(nom):
    """Assombrit l'image"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, y),
                (0, 0, 0),  # À compléter avec la couleur du pixel
            )

    dest.save(nom.with_stem(nom.stem + "-assombrie"))
    return dest


def action_rotation90(nom):
    """Pivoter la photo de 90° vers la gauche"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (hauteur, largeur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (0, 0),  # À compléter avec la position du pixel
                (rouge, vert, bleu),
            )

    dest.save(nom.with_stem(nom.stem + "-rotation90"))
    return dest


def action_inverse(nom):
    """Inverser les couleurs"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, y),
                (0, 0, 0),  # À compléter avec la couleur du pixel
            )

    dest.save(nom.with_stem(nom.stem + "-inverse"))
    return dest


def action_bizarre(nom):
    """Produit une image bizarre à partir de l'image de départ"""
    source = ImageOps.exif_transpose(Image.open(nom).convert("RGB"))
    largeur = source.width
    hauteur = source.height
    dest = Image.new("RGB", (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            rouge, vert, bleu = source.getpixel((x, y))
            dest.putpixel(
                (x, y),
                (int(rouge * vert) % 256, 0, 0),  # À compléter avec la couleur du pixel
            )

    dest.save(nom.with_stem(nom.stem + "-bizarre"))
    return dest


################################################################################
# NE RIEN MODIFIER APRÈS CETTE LIGNE
################################################################################


def docstring(fonction):
    """Renvoit la docstring (nettoyée) de la fonction."""
    if not fonction.__doc__.strip():
        raise ValueError(
            "La fonction '{}' n'a pas de docstring.".format(fonction.__name__)
        )
    return fonction.__doc__.strip().split("\n")[0]


def liste_actions():
    """Renvoit la liste d'actions définies dans le fichier.

    Les actions sont les fonctions dont le nom commence par "action_".
    """
    return sorted(
        (
            objet
            for nom, objet in inspect.getmembers(sys.modules[__name__])
            if (inspect.isfunction(objet) and nom.startswith("action_"))
        ),
        key=operator.attrgetter("__name__"),
    )


def choix_image():
    """Fait choisir une image à l'utilisateur·ice, et renvoit son nom.

    Si l'utilisateur·ice valide sans rien écrire, renvoit l'image par défaut DEFAULT_IMAGE
    (si elle existe).
    """
    print("#" * 80)
    print("# Choix de l'image #")
    images = [
        name
        for name in sorted(pathlib.Path(".").iterdir())
        if str(mimetypes.guess_type(name)[0]).startswith("image")
    ]
    for i, image in enumerate(images):
        print(
            "[{numéro: >{longueur}}] {nom}".format(
                numéro=i,
                nom=image,
                longueur=len(str(len(images) - 1)),
            )
        )
    while True:
        choix = input(
            f"Quelle image traiter [{DEFAULT_IMAGE}] ? "
            if DEFAULT_IMAGE.exists()
            else "Quelle image traiter ?"
        )
        try:
            if (not choix) and DEFAULT_IMAGE.exists():
                return DEFAULT_IMAGE
            if 0 <= int(choix) < len(images):
                print()
                return images[int(choix)]
        except ValueError:
            pass
        print("Veuillez choisir un nombre entre 0 et {}.".format(len(images) - 1))


def choix_action():
    """Fait choisir une action à l'utilisateur·ice, et renvoit la fonction."""
    print("#" * 80)
    print("# Choix de l'action #")
    actions = liste_actions()
    for compteur, fonction in enumerate(actions):
        print(
            "[{numéro: >{longueur}}] {nom} : {description}".format(
                numéro=compteur,
                longueur=len(str(len(actions) - 1)),
                nom=fonction.__name__.split("_", maxsplit=1)[1],
                description=docstring(fonction),
            )
        )
    while True:
        choix = input("Quelle action effectuer ? ")
        try:
            if 0 <= int(choix) < len(actions):
                print()
                return actions[int(choix)]
        except ValueError:
            pass
        print("Veuillez choisir un nombre entre 0 et {}.".format(len(actions) - 1))


def main():
    """Fonction principale"""
    if len(sys.argv) == 2 and sys.argv[1] == "all":
        # Correction : On exécute *toutes* les actions
        source = choix_image()
        for action in liste_actions():
            logging.info("Exécution de %s…", action.__name__)
            try:
                action(source)
            except Exception as erreur:  # pylint: disable=broad-exception-caught
                logging.error("Erreur avec l'action %s : %s", action.__name__, erreur)
        sys.exit(1)

    # Exécution d'un seul algorithme, au choix
    source = choix_image()
    action = choix_action()

    action(source).show()


if __name__ == "__main__":
    main()
