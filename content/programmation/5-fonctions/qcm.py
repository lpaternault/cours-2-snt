def question(n):
  reponse = 0
  while reponse != n:
      reponse = int(input("Entrez le numéro de votre réponse. "))

  print("Bravo !")

print("Qui a dirigé l'équipe qui a écrit le système embarqué d'Apollo 11 ?")
print("1. Grace Hopper")
print("2. Mae Jemison")
print("3. Margaret Hamilton")
print("4. Steve Jobs")

question(3)

print("Qui a réussi à « casser » la machine Enigma, utilisée par les Allemands pour chiffrer des messages durant la seconde guerre mondiale ?")
print("1. Alan Turing")
print("2. Auguste Kerckhoffs")
print("3. Bertrand Russel")
print("4. Kurt Gödel")

question(1)

print("Qui est considéré comme l'inventeur du web ?")
print("1. Louis Pouzin")
print("2. Radia Perlman")
print("3. Tim Berners-Lee")
print("4. Vint Cerf")

question(3)
