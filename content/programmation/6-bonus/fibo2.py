from functools import lru_cache

@lru_cache()
def fibonacci(n):
    if n == 1:
        return 0
    if n == 2:
        return 1
    return fibonacci(n-1) + fibonacci(n-2)

print("Le cinquième terme est", fibonacci(5))
print("Le centième terme est", fibonacci(20))
