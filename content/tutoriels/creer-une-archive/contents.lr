---
title: Créer une archive au format `.zip` contenant plusieurs fichiers et dossiers.
---
description: Comment compresser des fichiers et dossiers en une archive au format `.zip`.
---
image: clamp.jpg
---
body:

{% from 'macros/images.html' import image with context %}
{% from 'macros/bootstrap.html' import alertstart, alertend with context %}

On souhaite créer une archive au format `.zip` contenant le fichier `article.html` et `article_fichiers`.

{{ alertstart(level="info", title="")}}
Ce tutoriel concerne Windows 11. Un tutoriel pour Windows 10 est disponible <a href="windows10">par ici</a>.
{{ alertend() }}

## Sélectionner les fichiers à mettre dans l'archive

- cliquez « dans le vide » sous les fichiers et dossiers à sélectionner ;
- sans relacher le bouton, faites glisser la souris ;
- éventuellement, pour ajouter ou supprimer individuellement des fichiers, cliquez sur chacun des autres fichiers ou dossier en maintenant la touche <kbd>Ctrl</kbd> du clavier enfoncée.

{{ image("selection.png") }}

## Compressez les fichiers

Faitez un clic droit sur un des fichiers sélectionnéz, puis choisissez `Compresser dans un fichier ZIP`.

{{ image("compresser.png") }}

## C'est bon

Un nouveau fichier `page.zip` a été créé. Vous pouvez double-cliquer dessus pour vérifier son contenu.

{{ image("zip.png") }}
