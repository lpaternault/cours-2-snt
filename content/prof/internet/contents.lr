title: Internet
---
image: /internet/fishing-nets-youghal.jpg
---
description: Séquence d'enseignement sur le thème « Internet » — Ressources pour le professeur.
---
sort_key: 1
---
toc: 1
---
body:

{% from 'macros/images.html' import image with context %}

Tous les supports pour les élèves : [internet.pdf](internet.pdf).

## Déroulement

- Séance 1 : [Topologie](topologie.pdf) ([source](topologie.tex))

   Remarques :

   - Différentes structures :

     - Le réseau « décentralisé » correspond au réseau téléphonique (du moins, avant qu'il ne soit numérisé).
     - Le réseau « centré » correspond (dans l'idée) à la télévision, ou à la diffusion d'un journal imprimé.

   - En répondant à la dernière question, je précise que c'est une bonne illustration du métier d'ingénieur, dont le travail est de trouver la meilleure solution à un problème soumis à plusieurs contraintes incompatibles (avec comme exemple la construction d'un pont routier au dessus d'une rivière, dont on veut qu'il soit proche de la sortie d'autoroute, mais sans faire exploser le traffic dans un petit village, tout en préservant une zone naturelle protégée, en diminuant les coût au maximum pour construire un ouvrage le plus solide et durable possible…).

   - Il est possible de faire le travail sur le réseau décrentralisé en classe entière (parce que c'est le plus compliqué à dessiner), puis diviser la classe en trois groupes pour travailler les suivants, avant une mise en commun. Cela permet de gagner du temps…

- Séance 2

  - Routage ([à distribuer](routage.pdf) ([source](routage.tex)) ; [à projeter](routage-diapo.pdf) ([source](routage-diapo.tex))). Voir aussi : [routage élastique](https://members.loria.fr/MDuflot/files/med/routage.html).

  - [Modèle TCP-IP](tcp-ip.pdf) ([source](tcp-ip.tex)) : En cours magistral, le professeur lit et commente le document. Il pourra être utile :

     - de faire un schéma illustrant le découpage sur un exemple (une petite phrase par exemple) ;
     - de mentionner qu'il existe d'autres protocoles qu'IP, comme [UDP](https://fr.wikipedia.org/wiki/User_Datagram_Protocol), qui n'attend pas d'accusé de réception, et est utile dans des cas où la vitesse est plus importante que la fiabilité (streaming vidéo par exemple) ;
     - etc.

  -  Réseau physique : traffic ; indépendances

     - [Câbles sous-marins](cables.pdf) ([source](cables.tex))
     - [Calculs de débit](debit.pdf) ([source](debit.tex))

- Séance 3 : **DNS :**
   - *Théorie :* Au tableau, le professeur recopie et explique le schéma suivant, introduit par *« Le DNS permet de faire le lien entre un nom de domaine (par exemple ``fr.wikipedia.org``) et une adresse IP (par exemple ``185.15.58.224``) »*.

     {{ image("dns.png") }}

   - *Pratique :* [TP](../../internet/dns)

- Séance 4

  - [Neutralité du net](neutralite.pdf) ([source](neutralite.tex))
  - Début du chapitre suivant…

- À la maison

  - [Réseaux pair-à-pair](../../internet/p2p) : À la maison, avec un [QCM](QCM/p2p.txt) sur Pronote (images : [1](QCM/p2p-graphe-1.png) [2](QCM/p2p-graphe-2.png) [3](QCM/p2p-graphe-3.png)).

## Autres activités

Voici les activités que je n'utilise plus parce qu'elles ne fonctionnaient pas, par manque de temps, parce qu'elles ont été conçues pour être utilisées à distance, etc.

- [Réseau Internet et Réseau physique](/internet/reseau)
- [Protocole TCP-IP et Routage](/internet/tcp-ip)
