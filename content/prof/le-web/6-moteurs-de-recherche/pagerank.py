#!/usr/bin/env python

"""Calcul du score page rank

Pour les pages décrites à l'activité :
https://snt.ababsurdo.fr/prof/le-web/6-moteurs-de-recherche/pagerank.pdf
"""

import random

# Au lieu d'être nommés A, B, C, ..., H, les sommets sont notés par les nombres 0, 1, 2, ..., 7.
# Une fois cette définition effectuée, on a `graphe[2] == [0, 3]`,
# c'est-à-dire que deux arêtes partent du sommet 2 pour aller vers les sommets 0 et 3,
# ou encore que deux arêtes partent du sommet C pour aller vers les sommets A et D.
graphe = [
        [3, 5], # Sommet A
        [0, 4, 6], # Sommet B
        [0, 3], # Sommet C
        [2, 5], # Sommet D
        [0, 6, 7], # Sommet E
        [1, 2, 3], # Sommet F
        [0, 1], # Sommet G
        [0, 2, 6], # Sommet H
        ]

# Nombre de répétitions
n = 1000

# Le sommet de départ est choisi aléatoirement
sommet = random.randrange(0, len(graphe))

# Au départ, chaque sommet a été visité 0 fois
# La notation `[0] * len(graphe)` est équivalente à `[0, 0, 0, 0, 0, 0, 0, 0]`.
visites = [0] * len(graphe)

# On répète la simulation
for i in range(n):
    # On choisit aléatoirement un nouveau sommet parmi ceux accessibles depuis `sommet`
    sommet = random.choice(graphe[sommet])
    # On augmente le compteur de visite
    visites[sommet] += 1

# On affiche les séquences calculées
for i in range(len(graphe)):
    print(f"Sommet {i}: {visites[i]/n}")
