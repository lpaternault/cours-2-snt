_discoverable: false
---
title: Données personnelles (1h30)
---
toc: 2
---
body:

{% from 'macros/bootstrap.html' import alertstart, alertend with context %}

{{ alertstart(level="info", title="Remarques")}}
<ul>
<li>Lire <a href="https://ababsurdo.fr/blog/20210403-organisation-de-debats-en-snt/">mon bilan de cette séance</a>.</li>
<li>Convaincu par <a href="https://groupes.renater.fr/sympa/arc/sciences-numeriques-technologie/2021-04/msg00010.html">les arguments de Julien Peccoud</a>, je fais désormais cette séance sous la forme d'un <a href="../debat-mouvant">débat mouvant</a>.</li>
</ul>
{{ alertend() }}

Les gens non concernés sont généralement peu réceptifs aux arguments en faveur d'une protection des données (je n'ai rien à cacher). L'idée est de forcer une partie de la classe à défendre ce point de vue dans un débat.

> Si je n'ai rien à me reprocher, je n'ai rien à cacher.

Le déroulement du débat est inspiré de : [Réussir un débat](https://sesame.apses.org/index.php?option=com_content&view=article&id=84&Itemid=235).

## Déroulement de la séance

### Séance précédente

Le principe de la séance est annoncé dans les grandes lignes à la séance précédente (le thème du débat). Le [corpus de textes](../debat-mouvant/corpus.pdf) ([sources](../debat-mouvant/corpus.tar.xz)) est distribué (une page par élève ; plusieurs élèves peuvent avoir la même page).

### Devoirs

Chez eux, pour préparer le débat, les élèves doivent lire la page qui leur a été distribuée, et préparer des arguments *pour* et *contre* la phrase de débat.

### Séance

- 5' Diffusion de la vidéo [Datagueule 15 : Big data](https://www.youtube.com/watch?v=5otaBKsz7k4) pour introduire le débat.

- 10' Explication du fonctionnement et distribution des rôles : 

  - [président·e](roles/president.pdf) ([source](roles/equipier.tex)) : Le ou les président·e·s s'assurent que les règles sont respectées (dans l'idéal, le professeur est en retrait et n'intervient pas) ;
  - [montre](roles/montre.pdf) ([source](roles/equipier.tex))) : ils assistent le président en s'occupant du chronométrage ;
  - [jury](roles/jury.pdf) ([source](roles/equipier.tex)) : ils évaluent les équipiers (qui débattent) en mettant des points et pénalités en fonction du respect des règles ;
  - [équipier·e](roles/equipier.pdf) ([source](roles/equipier.tex)) : ils débattent. Seuls deux personnes dans chaque équipes (les orateur·ice·s, qui peuvent changer pendant le débat, interviennent ; les autres préparent les arguments avec eux, et les épaulent pendant les pauses).

- 20' Préparation du débat : les deux équipes affutent leurs arguments. Pendant ce temps, avec le jury :

  - je détaille leur rôle ;
  - je les fais mettre la salle en place ;
  - nous assistons à [un exposé](https://snt.ababsurdo.fr/exposes/) (du coup, j'ai bien pris soin de constituer les équipes en plaçant les élèves qui présentent l'exposé dans le jury).

- 30' Débat :

  - Trois rounds de cinq minutes de débats entre les quatre « champions ».
  - Trois minutes de pause entre chaque round, durant lesquels les équipes peuvent se concerter.
  - Chaque équipe a droit à un temps mort d'une minute par round.

- 10' Décompte des points et bilan.

Cela fait un total d'une heure et quart, sur une séance d'une heure et demie. Il faut donc être prêt à meubler, ou à introduire le chapitre suivant.
