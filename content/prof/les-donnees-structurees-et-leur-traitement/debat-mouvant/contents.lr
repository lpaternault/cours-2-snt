title: Données personnelles : Débat mouvant (1h30)
---
sort_key: 6
---
body:

Les gens non concernés sont généralement peu réceptifs aux arguments en faveur d'une protection des données (je n'ai rien à cacher). Le but de cette séance est de débattre de ce sujet.

Le déroulement du débat est inspiré d'un travail de [Julien Peccoud](https://groupes.renater.fr/sympa/arc/sciences-numeriques-technologie/2021-04/msg00010.html).

## Déroulement de la séance

- Introduction

    - (Optionnel) Diffusion de la vidéo [Datagueule 15 : Big data](https://www.youtube.com/watch?v=5otaBKsz7k4) pour introduire le débat.
    - Explication du contexte : « Que nous le voulions ou non, des données sont sans arrêt collectées sur nous : caméras de surveillance dans la rue (gérées par la police) ou dans les magasins (gérées par des sociétés de sécurité privées), trajets de bus et de train (avec nos cartes nominatives), achats (avec nos cartes de fidélité), navigation sur Internet, données scolaires (avec le logiciel de vie scolaire), etc. Certaines personnes ne sont pas d'accord avec la collecte d'autant de données ; d'autres affirment au contraire que "Si je n'ai rien à me reprocher, je n'ai rien à cacher" »

- Premier débat

    - La phrase « Si je n'ai rien à me reprocher, je n'ai rien à cacher » est inscrite au tableau.
    - Explications des règles :

    - Les tables et chaises sont poussées aux bords de la salle.
        - Une ligne (imaginaire) est tracée dans la salle, représentant le degré d'accord avec la phrase : de « complètement d'accord » à un bout de la ligne, à « pas du tout d'accord » à l'autre bout, avec toutes les nuances entre les deux.
        - Les élèves doivent se positionner quelque part sur cette ligne.
        - Les élèves sont invités à donner leurs arguments, c'est-à-dire à expliquer pourquoi ils et elles se sont placé·e·s à cet endroit là. Si des personnes sont d'accord avec un argument proposé (même si cet argument n'est pas suffisant pour les faire changer d'avis), elles se déplacent.

- Recherche d'arguments

    - Assez vite, nous arrivons à cours d'arguments, ou nous nous rendons compte que les *arguments* proposés sont en fait des opinions, ou des connaissances approximatives.
    - On explique la différence entre argument et opinion.
    - Je distribue ce [corpus de textes](../debat-mouvant/corpus.pdf) ([sources](../debat-mouvant/corpus.tar.xz)) (une page par élève ; plusieurs élèves peuvent avoir la même page). Il contient des documents (principalement des articles de journaux, mais pas seulement) desquels les élèves vont pouvoir tirer des arguments pour ou contre l'affirmation.
    - Les élèves doivent lire leur document (seul·e et en silence), et chercher et noter (pour les partager par la suite) dans ce document des arguments *pour* ou *contre* la phrase, indépendamment de leur opinion personnelle. Pendant ce temps, il faut circuler pour accompagner les élèves : certains textes sont plus difficiles que d'autres, et il faut souvent aider les élèves à formuler l'argument, car ils ont du mal à formuler un argument à partir d'une anecdote.

      C'est la phase la plus longue, qui peu prendre une demi-heure.
    - Les élèves partagent les arguments, que je note au tableau, en essayant de faire des liens pour les arguments similaires.

- Débats suivants : nous enchaînons avec trois autres débats, qui sont faits assez rapidement (sauf si l'heure permet de prendre davantage de temps).

    - « Peu importe que les entreprises utilisent mes données, je n'ai rien à cacher »
    
      Précision : on suppose qu'aucune faille sécurité ne révèle des données privées (mais les entreprises peuvent utiliser les données privées qu'elles ont collectées, comme les rendez-vous médicaux par Doctolib, ou les voyages en train par la SNCF).

    - « Peu importe ce que l'on trouve sur moi en ligne, je n'ai rien à cacher »
    
      Précision : il s'agit ici des données publiques, et non pas des données privées révélées accidentellement (contenus postés sur les réseaux sociaux par exemple).

    - « Peu m’importe que toutes mes actions et conversations soient enregistrées, même chez moi, je n’ai rien à cacher »
      
      Précision : Comme dans les deux cas précédents, les données privées restent privées.

    À la fin de ce dernier débat, je leur demande si la situation évoquée existe réellement, et certains remarquent que c'est ce que font les assistants comme [Alexa](https://fr.wikipedia.org/wiki/Amazon_Alexa), [Siri](https://fr.wikipedia.org/wiki/Amazon_Alexa) ou [Google Home](https://fr.wikipedia.org/wiki/Google_Home).

    J'ai eu des élèves défaitistes affirmant qu'on « ne peut rien faire », donc autant accepter cette situation, donc suivant le temps restant (même si je n'ai pas eu cette remarque), je leur demande ce que l'on peut faire pour lutter contre cela si on n'est pas d'accord. J'attends les réponses suivantes :

    - action personnelle : ne pas utiliser ce genre de produits ;
    - action militante : s'engager dans des associations qui luttent contre ce genre de pratique ;
    - action politique : voter ou se faire élire pour changer les lois et interdire ce genre de pratiques.
