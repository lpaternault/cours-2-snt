import pandas
import matplotlib.pyplot as plt

prenoms = pandas.read_csv("dpt2022.csv", sep=";")
groupes = prenoms.drop(columns=["dpt"]).loc[(prenoms["annais"] != "XXXX") & (prenoms["preusuel"] != "_PRENOMS_RARES")].groupby(["annais", "preusuel", "sexe"], as_index=False).sum()

année = int(input("Année de naissance ? "))
élèves = tuple(nom.upper().strip() for nom in input("Liste des prénoms séparés par des espaces ? ").split(" "))

print(f"Prénoms les plus populaires en {année} :")
for sexe in (1, 2):
    print(groupes.loc[(groupes['sexe'] == sexe) & (groupes['annais'] == str(année)), :].sort_values(by="nombre").tail(1))
print()

for sexe in (1, 2):
    for nom in élèves:
        print(f"Nombre d'enfants de sexe {sexe} nés avec le prénom {nom} en {année}:")
        print(groupes.loc[(groupes["sexe"] == sexe) & (groupes['annais'] == str(année)) & (groupes["preusuel"] == nom), :].sort_values(by="nombre"))
print()

for nom in élèves:
    print(f"Le prénom {nom} a été le plus populaire en :")
    try:
        maximum = int(groupes.loc[(groupes["preusuel"] == nom), :].sort_values(by="nombre").tail(1)["nombre"].iloc[0])
        print(groupes.loc[(groupes["preusuel"] == nom) & (groupes["nombre"] == maximum), :])
    except IndexError:
        print("Pas assez de personnes avec ce prénom…")

for sexe in (1, 2):
    for nom in élèves:
        print(f"Courbe de {nom} de sexe {sexe}")
        recherche = groupes.loc[(groupes['preusuel'] == nom) & (groupes['sexe'] == sexe), :]
        recherche.plot(x="annais", y="nombre")
        plt.show()


