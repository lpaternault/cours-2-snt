import pandas
parole = pandas.read_csv("20190308-years.csv")

print("# Moyennes des moyennes du temps de parole des femmes")
for année in  (1999, 2009, 2019):
    print(année, parole.loc[(parole['year'] == année), ["women_expression_rate"]]["women_expression_rate"].mean())

print("# Chaînes les plus ou les moins égalitaires")
recherche = parole.loc[(parole['year'] == 2019), ["channel_name", "women_expression_rate"]]
tri = recherche.sort_values(by="women_expression_rate")
print(tri.head(n=5))
print(tri.tail(n=5))
