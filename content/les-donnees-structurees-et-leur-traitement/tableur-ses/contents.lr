title: SES et Tableur
---
sort_key: 3
---
image: computer-science-diploma.jpg
---
morehead:

<link rel="stylesheet" href="/static/numbered-h2-h3.css">
<link rel="stylesheet" href="/static/sublist.css">
---
body:

{% from 'macros/bootstrap.html' import alertstart, alertend with context %}

Le but de l'activité est de compléter [ce tableau](tableau.pdf), qui vous a été distribué par le professeur, pour ensuite l'analyser en cours de SES.

Les données vont être cherchées depuis le site de l'INSEE.

# Taux de chômage

Nous allons comparer dans cette partie le taux de chômage des hommes et des femmes.

0. Allez sur la page [L'essentiel sur... le chômage](https://www.insee.fr/fr/statistiques/4805248), et cherchez les données sur le chômage par sexe, en 2022.
0. Recopiez ces données dans la colonne **Ensemble** des lignes **Taux de chômage** de votre tableau.

# Plus haut diplôme obtenu

Nous allons comparer dans cette partie le plus haut diplôme obtenu par les hommes et les femmes.

0. Rendez-vous sur la page [Diplôme le plus élevé selon l'âge et le sexe en 2022](https://www.insee.fr/fr/statistiques/2416872), et regardez le tableau.
0. Complétez votre tableau en reportant les valeurs lues sur cette page. Attention à bien recopier les valeurs qui correspondent à *l'ensemble* des âges des hommes et des femmes, et non pas seulement à une tranche d'âge en particulier.

# Salaire médian

Nous allons comparer dans cette partie les salaires médians des hommes et des femmes, selon le plus haut diplôme obtenu.

0. Allez sur la page [Activité, emploi et chômage en 2020 et en séries longues](https://www.insee.fr/fr/statistiques/5359503), et téléchargez le fichier *SAL01 – Salaires mensuels nets selon le sexe, l'âge et le diplôme*, au format ``xlsx``.
0. Ouvrez-le avec un tableur.
0. Repérez la colonne correspondant au **salaire médian** de **l'ensemble des femmes**, et supprimez toutes les autres colonnes sauf la première (liste des diplômes). Renommez cette colonne pour bien préciser qu'il s'agit du salaire des **femmes**.
0. Repérez la colonne correspondant au **salaire médian** de **l'ensemble des hommes**, et copiez cette colonne à côté de la colonne de la question précédente (salaire des femmes). Renommez-la pour bien préciser qu'il s'agit du salaire des **hommes**.
0. Nous allons maintenant calculer la différence relative entre ces salaires.

  0. Entrez, dans une nouvelle cellule de la ligne des personnes sans diplôme, la formule permettant de calculer la différence de salaire entre les hommes et les femmes.
  0. Modifiez cette formule pour diviser le résultat par le salaire des hommes.
  0. Modifiez le format de la cellule pour afficher le résultat en pourcentages.
  0. Faites glisser la formule sur les autres cellules de la colonne pour refaire le même calcul pour tous les diplômes.

0. Reportez les valeurs trouvées (salaire des femmes, salaire des hommes, différence relative) dans votre tableau.
