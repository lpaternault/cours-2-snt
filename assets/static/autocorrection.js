function _autocorrection_valide(id, réponse) {
  document.getElementById(`answer-${id}`).classList.remove("is-invalid")
  document.getElementById(`answer-${id}`).classList.remove("is-valid")
  document.getElementById(`answer-${id}`).classList.add("is-valid")
  document.getElementById(`feedback-${id}`).innerHTML = ""
  document.getElementById(`answer-${id}`).value = réponse
  document.getElementById(`answer-${id}`).disabled = true
  document.getElementById(`button-${id}`).disabled = true
}
function _autocorrection_invalide(id, réponse) {
  document.getElementById(`answer-${id}`).classList.remove("is-invalid")
  document.getElementById(`answer-${id}`).classList.remove("is-valid")
  document.getElementById(`answer-${id}`).classList.add("is-invalid")
  document.getElementById(`feedback-${id}`).innerHTML = `La réponse <em>« ${document.getElementById(`answer-${id}`).value} »<em> n'est pas correcte…`
}

function autocorrection_verifier(id, type, réponse, erreur=0) {
  if (type == "prefix") {
    if (document.getElementById(`answer-${id}`).value.trim().toLowerCase().indexOf(réponse.trim().toLowerCase()) == 0) {
      _autocorrection_valide(id, réponse)
    } else {
      _autocorrection_invalide(id, réponse)
    }
  } else if (type == "number") {
    if (Math.abs(Number(document.getElementById(`answer-${id}`).value) - Number(réponse)) <= erreur) {
      _autocorrection_valide(id, réponse)
    } else {
      _autocorrection_invalide(id, réponse)
    }
  } else if (type == "choice") {
    if (réponse.includes(document.getElementById(`answer-${id}`).value.trim().toLowerCase())) {
      _autocorrection_valide(id, document.getElementById(`answer-${id}`).value.trim().toLowerCase())
    } else {
      _autocorrection_invalide(id, réponse)
    }
  } else {
    if (réponse.trim().toLowerCase() == document.getElementById(`answer-${id}`).value.trim().toLowerCase()) {
      _autocorrection_valide(id, réponse)
    } else {
      _autocorrection_invalide(id, réponse)
    }
  }
  return false
}
